package com.example.orbeon.route;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.cmis.CamelCMISConstants;
import org.apache.chemistry.opencmis.commons.PropertyIds;
import org.springframework.stereotype.Component;

@Component
public class MyRoute extends RouteBuilder{

	@Override
	public void configure() throws Exception {
		from("file:inputFolder?noop=true")
		.to("xslt:xslt/trychargereferral4.xsl")/*.process(new Processor(){

			@Override
			public void process(Exchange exchange) throws Exception {
				exchange.getIn().getHeaders().put(PropertyIds.CONTENT_STREAM_MIME_TYPE, "text/plain; charset=UTF-8");
                exchange.getIn().getHeaders().put(PropertyIds.NAME, exchange.getIn().getHeader(Exchange.FILE_NAME));
                exchange.getIn().getHeaders().put(CamelCMISConstants.CMIS_FOLDER_PATH, "/OrbeanForm");
				
			}
			
		})*/
		//.to("cmis://http://localhost:8093/alfresco/cmisatom?username=admin&password=admin");
		.to("file:src/main/resources/OUTPUT");
		//.to("cmis://http://localhost:8096/alfresco/cmisatom?repositoryId=371554cd-ac06-40ba-98b8-e6b60275cca7&username={{username}}&password={{password}}");
		
/*		from("direct:a").setProperty(Exchange.CONTENT_TYPE, constant("text/plain"))
		.setProperty(CmisConstants.CMIS_DOCUMENT_NAME, constant("testcmisdocument")) 
		.to("cmis://http://admin:admin@192.168.56.101:8080/alfresco/service/cmis#/testcamel/subfolder/subsubfolder")*/
		/*from("direct:a").setProperty(Exchange.CONTENT_TYPE, constant("text/plain")) 
		.setProperty(CmisConstants.CMIS_DOCUMENT_NAME, constant("testcmisdocument")) 
		.to("cmis://http://admin:admin@192.168.56.101:8080/alfresco/service/cmis#/testcamel/subfolder/subsubfolder")*/
	}

}
