<?xml version="1.0" encoding="utf-8" ?>
<xsl:stylesheet version="1.0"
    xmlns:cr-doc="http://ojbc.org/IEPD/Exchange/ChargeReferral/1.0"
    xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd"
    xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xmlns:lexspd="http://usdoj.gov/leisp/lexs/publishdiscover/3.1"
	xmlns:ndexia="http://fbi.gov/cjis/N-DEx/IncidentArrest/2.1"
	xmlns:nc="http://niem.gov/niem/niem-core/2.0"
	xmlns:ns="http://niem.gov/niem/structures/2.0"
	xmlns:lexs="http://usdoj.gov/leisp/lexs/3.1"
	xmlns:ir="http://ojbc.org/IEPD/Exchange/IncidentReport/1.0"
	xmlns:lexsdigest="http://usdoj.gov/leisp/lexs/digest/3.1"
	xmlns:j="http://niem.gov/niem/domains/jxdm/4.0"
	xmlns:lexslib="http://usdoj.gov/leisp/lexs/library/3.1"
	xmlns:inc-ext="http://ojbc.org/IEPD/Extensions/IncidentReportStructuredPayload/1.0"
	xmlns:xsl = "http://www.w3.org/1999/XSL/Transform">
 
    <xsl:output method="xml" indent="yes"/>
    <xsl:strip-space elements="*"/>
 
     <xsl:template match="cr-doc:ChargeReferral">
   <!--  Namespace nodes:
         <xsl:for-each select="namespace::*">
             <xsl:value-of select="name()"/><xsl:text> </xsl:text>
         </xsl:for-each> -->
         <xsl:apply-templates/>
     </xsl:template>
     
     <xsl:template match="lexspd:doPublish">
    <!-- Namespace nodes:
         <xsl:for-each select="namespace::*">
             <xsl:value-of select="name()"/><xsl:text> </xsl:text>
         </xsl:for-each> -->
         <xsl:apply-templates/>
     </xsl:template> 
     
      <!-- IdentityTransform -->
  <xsl:template match="/">
    <xsl:copy>
      <xsl:apply-templates/>
    </xsl:copy>
  </xsl:template>
 
 <xsl:template match="cr-doc:ChargeReferral/lexspd:doPublish/lexs:PublishMessageContainer">
   <xsl:for-each select="lexs:PublishMessage/lexs:DataItemPackage"> 
       	<form>
			<Incident>
				<incCaseNo>				
			  		<xsl:value-of select="lexs:Digest/lexsdigest:EntityActivity/nc:Activity/nc:ActivityIdentification/nc:IdentificationID"/>			
				</incCaseNo>				
				<incType>				
			  		<xsl:apply-templates/>			
				</incType>
				<incCustody>				
			  		<xsl:apply-templates/>			
				</incCustody>				
				<incDateTime>
					<xsl:value-of select="lexs:Digest/lexsdigest:EntityActivity/nc:Activity/nc:ActivityDate/nc:Date"/> 
				</incDateTime>
				<incDateText>				
			  		<xsl:apply-templates/>		
				</incDateText>
				<incCounty>				
			  		<xsl:apply-templates/>				
				</incCounty> 				
				<incAgency>
					 <xsl:value-of select="lexs:PackageMetadata/lexs:DataOwnerMetadata/lexs:DataOwnerIdentifier/nc:OrganizationName"/>
				</incAgency>
				
			   <xsl:for-each select="lexs:Digest/lexsdigest:EntityPerson/lexsdigest:Person">
				  <xsl:if test = "@ns:id = 'OFFICER_1'"> 
				  <offrInChr>
				    <xsl:value-of select="nc:PersonName/nc:PersonFullName"/> 				   
				  </offrInChr>
				  </xsl:if>
				</xsl:for-each>
				
				<dv>				
			  		<xsl:apply-templates/>			
				</dv>
				<incClass>				
			  		<xsl:apply-templates/>			
				</incClass>
				<schoolCode>				
			  		<xsl:apply-templates/>			
				</schoolCode>
				<distCourt>				
			  		<xsl:apply-templates/>			
				</distCourt>
				<vehicleUsed>				
			  		<xsl:apply-templates/>			
				</vehicleUsed>
				<incReSub>				
			  		<xsl:apply-templates/>			
				</incReSub>
				<priorAtty>				
			  		<xsl:apply-templates/>			
				</priorAtty>
				
				<xsl:for-each select="lexs:Digest/lexsdigest:EntityLocation/nc:Location">
				  <xsl:if test = "@ns:id = 'LOCATION_1'"> 
				  <incAddrText>
				    <xsl:value-of select="nc:LocationAddress/nc:StructuredAddress/nc:LocationStreet/nc:StreetFullText "/> 				   
				  </incAddrText>
				  <incAddr1>
				    <xsl:value-of select="nc:LocationAddress/nc:StructuredAddress/nc:LocationStreet/nc:StreetFullText "/> 				   
				  </incAddr1>
				  <incAddr2>
				    <xsl:value-of select="nc:LocationAddress/nc:StructuredAddress/nc:LocationStreet/nc:StreetNumberText "/> 				   
				  </incAddr2>
				  <incCountry>
				   <xsl:apply-templates/>				   
				  </incCountry>
				  <incState>
				    <xsl:value-of select ="nc:LocationAddress/nc:StructuredAddress/nc:LocationStateName " /> 				   
				  </incState>
				  <incZip>
				    <xsl:value-of select ="nc:LocationAddress/nc:StructuredAddress/nc:LocationPostalCode" /> 				   
				  </incZip>
				  <incCity>
				    <xsl:value-of select ="nc:LocationAddress/nc:StructuredAddress/nc:LocationCityName" /> 				   
				  </incCity>
				  <incLocCode>
				    <xsl:apply-templates/>				   
				  </incLocCode>
				  <incLocText>
				    <xsl:value-of select ="nc:LocationName" /> 				   
				  </incLocText>
				  </xsl:if>
				</xsl:for-each>
				
				<uploadDocs>
					<incDocType>									
						<xsl:apply-templates/>			
					</incDocType>
					<incUploadDoc>									
						<xsl:apply-templates/>			
					</incUploadDoc>		
				</uploadDocs>
				
			</Incident> 				
			
			 <DefendantList>			
				<Defns>				
				<Defendant>				
				<xsl:for-each select="lexs:Digest/lexsdigest:EntityPerson/lexsdigest:Person"> 
					<xsl:if test = "@ns:id = 'SUBJECT_00064117'"> 
					
						<sid>				
						  	<xsl:value-of select="nc:PersonSSNIdentification/nc:IdentificationID"/>			
						</sid>
						
						<tcn>				
						  	<xsl:apply-templates/>			
						</tcn>
						
						<defnFName>				
						  	<xsl:value-of select="nc:PersonName/nc:PersonGivenName"/>			
						</defnFName>
						
						<defnMName>				
						  	<xsl:value-of select="nc:PersonName/nc:PersonMiddleName"/>			
						</defnMName>
						
						<defnLName>				
						  	<xsl:value-of select="nc:PersonName/DEBLOCK"/>			
						</defnLName>
						
						<defnSuffix>				
						  	<xsl:apply-templates/>			
						</defnSuffix>
						
						<defnAlias>				
						  	<xsl:apply-templates/>			
						</defnAlias>
						
						<defnDOB>				
						  	<xsl:value-of select="nc:PersonBirthDate/nc:Date"/>			
						</defnDOB>
						
						<defnAge>				
						  	<xsl:apply-templates/>			
						</defnAge>
						
						<defnGender>				
						  	<xsl:value-of select="nc:PersonSexCode"/>			
						</defnGender>
						
						<defnRace>				
						  	<xsl:value-of select="nc:PersonRaceText"/>			
						</defnRace>
						
						<primLang>				
						  	<xsl:apply-templates/>				
						</primLang>
						
						<intrptrReq>				
						  	<xsl:apply-templates/>				
						</intrptrReq>
						
						<defnSSN>				
						  	<xsl:value-of select="nc:PersonSSNIdentification/nc:IdentificationID"/>			
						</defnSSN>
						
						<defnLicSt>				
						  	<xsl:value-of select="j:PersonAugmentation/nc:DriverLicense/nc:DriverLicenseIdentification/nc:IdentificationJurisdictionText"/>			
						</defnLicSt>
						
						<defnDLN>				
						  	<xsl:value-of select="j:PersonAugmentation/nc:DriverLicense/nc:DriverLicenseIdentification/nc:IdentificationID"/>			
						</defnDLN>
						
						<defnLicType>				
						  	<xsl:apply-templates/>			
						</defnLicType>
						
						<defnVehicle>				
						  	<xsl:apply-templates/>			
						</defnVehicle>
						
						<schoolEmp>				
						  	<xsl:apply-templates/>			
						</schoolEmp>
						
						<arrestDate>				
						  	<xsl:apply-templates/>			
						</arrestDate>
						
						<forfeitDate>				
						  	<xsl:apply-templates/>		
						</forfeitDate>
						
						<dna>				
						  	<xsl:apply-templates/>		
						</dna>
						
						<cpl>				
						  	<xsl:apply-templates/>			
						</cpl>
						
						<weaponUsed>				
						  	<xsl:apply-templates/>		
						</weaponUsed>
						
						<infPolice>				
						  	<xsl:apply-templates/>			
						</infPolice>
						
						<permitNo>				
						  	<xsl:apply-templates/>			
						</permitNo>
						
						<weaponCode>				
						  	<xsl:apply-templates/>			
						</weaponCode>
						</xsl:if>
				</xsl:for-each>
						
					<xsl:for-each select="lexs:Digest/lexsdigest:EntityLocation/nc:Location">
				  	<xsl:if test = "@ns:id = 'LOCATION_2'"> 
						
						<defnAddrText>				
						  	<xsl:value-of select="nc:LocationAddress/nc:StructuredAddress/nc:LocationStreet/nc:StreetFullText"/>			
						</defnAddrText>
						
						<defnLicRegEx>				
						  	<xsl:apply-templates/>			
						</defnLicRegEx>
						
						<defnAddr1>				
						  	<xsl:value-of select="nc:LocationAddress/nc:StructuredAddress/nc:LocationStreet/nc:StreetNumberText"/>			
						</defnAddr1>
						
						<defnAddr2>				
						  	<xsl:value-of select="nc:LocationAddress/nc:StructuredAddress/nc:LocationStreet/nc:StreetName"/>			
						</defnAddr2>
						
						<defnCountry>				
						  	<xsl:apply-templates/>		
						</defnCountry>
						
						<defnState>				
						  	<xsl:value-of select="nc:LocationAddress/nc:StructuredAddress/nc:LocationStateName"/>			
						</defnState>
						
						<defnZip>				
						  	<xsl:value-of select="nc:LocationAddress/nc:StructuredAddress/nc:LocationPostalCode"/>			
						</defnZip>
						
						<defnCity>				
						  	<xsl:value-of select="nc:LocationAddress/nc:StructuredAddress/nc:LocationCityName"/>			
						</defnCity>
						
						</xsl:if>
				</xsl:for-each>  	
				
						
				</Defendant>
			<DefnPhones>
					<xsl:for-each select="lexs:Digest/lexsdigest:EntityTelephoneNumber"> 
					<xsl:if test = "@ns:id = 'EContact_16383'"> 
					
				        <defnPhType>				
						  	<xsl:apply-templates/>				
						</defnPhType>
						
						<defnPhNum>				
						  	<xsl:value-of select="lexsdigest:TelephoneNumber/nc:FullTelephoneNumber/nc:TelephoneNumberFullID"/>		
						</defnPhNum>
						
						</xsl:if>
						</xsl:for-each>
				
			</DefnPhones>					
						
			<Charges>
					<xsl:for-each select="lexs:StructuredPayload/inc-ext:IncidentReport/inc-ext:Incident/inc-ext:Charge "> 
					 <xsl:if test = "@ns:id = 'CHARGE_1'">  
			
						<reqCharge>				
						  	<xsl:value-of select="j:ChargeText"/>			
						</reqCharge>
						
						<acs>				
						  	<xsl:apply-templates/>			
						</acs>
						</xsl:if>
					</xsl:for-each>
			</Charges>	
			
						<defnData>				
						  	<xsl:apply-templates/>			
						</defnData>
						
						<defnId>				
						  	<xsl:apply-templates/>			
						</defnId>				
					
				</Defns>
							
			</DefendantList>
			
			
			<WitnessList>
				<Witns>
					<Witness>
					<xsl:for-each select="lexs:Digest/lexsdigest:EntityPerson/lexsdigest:Person "> 
					 <xsl:if test = "@ns:id = 'WITNESS_00064116'">
						<witnType>
							<xsl:apply-templates/>
						</witnType>
						<witnAgency>
							<xsl:apply-templates/>
						</witnAgency>
						<witnBarNo>
							<xsl:apply-templates/>
						</witnBarNo>
						<witnFName>
							<xsl:value-of select="nc:PersonName/nc:PersonGivenName"/>
						</witnFName>
						<witnMName>
							<xsl:value-of select="nc:PersonName/nc:PersonMiddleName"/>
						</witnMName>
						<witnLName>
							<xsl:value-of select="nc:PersonName/nc:PersonSurName"/>
						</witnLName>
						<witnSuffix>
							<xsl:apply-templates/>
						</witnSuffix>
						<witnGender>
							<xsl:value-of select="nc:PersonSexCode"/>
						</witnGender>
						<witnRace>
							<xsl:value-of select="nc:PersonRaceText"/>
						</witnRace>
						<witnDOB>
							<xsl:value-of select="nc:PersonBirthDate/nc:Date"/>
						</witnDOB>
						<witnAge>
							<xsl:apply-templates/>
						</witnAge>
						<witnSSN>
							<xsl:apply-templates/>
						</witnSSN>
						<witnLicSt>
							<xsl:value-of select="j:PersonAugmentation/nc:DriverLicense/nc:DriverLicenseIdentification/nc:IdentificationJurisdictionText"/>
						</witnLicSt>
						<witnDLN>
							<xsl:value-of select="j:PersonAugmentation/nc:DriverLicense/nc:DriverLicenseIdentification/nc:IdentificationID"/>
						</witnDLN>
						<witnLicType>
							<xsl:apply-templates/>
						</witnLicType>
						<witnAddrText>
							<xsl:apply-templates/>
						</witnAddrText>
						<witnLicRegEx>
							<xsl:apply-templates/>
						</witnLicRegEx>
						</xsl:if>
						</xsl:for-each>
					<xsl:for-each select="lexsdigest:EntityLocation/nc:Location"> 
					 <xsl:if test = "@ns:id = 'LOCATION_3'">
						<witnAddr1>
							<xsl:value-of select="nc:LocationAddress/nc:StructuredAddress/nc:StreetNumberText"/>
						</witnAddr1>
						<witnAddr2>
							<xsl:value-of select="nc:LocationAddress/nc:StructuredAddress/nc:StreetName"/>
						</witnAddr2>
						<witnCountry>
							<xsl:apply-templates/>
						</witnCountry>
						<witnSate>
							<xsl:value-of select="nc:LocationAddress/nc:StructuredAddress/nc:LocationStateName"/>
						</witnSate>
						<witnZip>
							<xsl:value-of select="nc:LocationAddress/nc:StructuredAddress/nc:LocationPostalCode"/>
						</witnZip>
						<witnCity>
							<xsl:value-of select="nc:LocationAddress/nc:StructuredAddress/nc:LocationCityName"/>
						</witnCity>
					</xsl:if>
					</xsl:for-each>
					</Witness>
					
					<WitnPhones>
						<witnPhType>
							<xsl:apply-templates/>
						</witnPhType>						
						<witnPhNum>
							<xsl:apply-templates/>
						</witnPhNum>						
					</WitnPhones>
				</Witns>
			</WitnessList>	 
			
		
     	</form>
     </xsl:for-each>
     
    </xsl:template>     
    
 
     <xsl:template match="*"/>
 
</xsl:stylesheet>